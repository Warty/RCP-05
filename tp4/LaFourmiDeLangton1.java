package tp4;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JPanel;
public class LaFourmiDeLangton1 extends JFrame  implements ActionListener
{
	//Champs object placer dans la fenetre du Container
	public JLabel nb_tour;
	public JButton next;
	//Fin 
	public Container panneau; //Panneau dans laquelle on va placer nos �l�ments
	public int cpt; //Compteur pour pouvoir indiquer le nombre de Tour
	public int formatfourmi; //Par d�fauts dans initFourmie sa affiche 0
	//Dimension du Plateau et de la Fourmi//
	public int dimension = 100; //Attributs Dimension initialis� � 10
	public int x = (dimension/2); //Affichage de x au millieu
	public int y = (dimension/2); //Affichage de y au millieu
	//Fin//
	//Parametre de d�placement de la fourmi
	public int direction=0;
	public JTextField plateau[][];
	Fourmi m; //Pour Init
	
	//Ajout de la m�thode creerPlateau qui va se generer lorsqu'on va l'ajouter � notre Container
	public JPanel creerPlateau()
	{
		JPanel p = new JPanel();
		plateau = new JTextField[dimension][dimension];
		p.setLayout(new GridLayout(dimension, dimension));
		for (int i = 0; i < dimension; i++)
			{
			   for (int j = 0; j < dimension; j++) {
				JTextField s = new JTextField();
				// plateau[i][j].setBackground(Color.WHITE);
				plateau[i][j] = s;
				plateau[i][j].setBackground(Color.white); //Permet de d�finir toutes les case blanches
				p.add(s);
			}
	}
			return p;
	}
	//Fin Exercice 1.3
	
	//Exercice 1.1 //Ajout du Constructeur
	public LaFourmiDeLangton1() 
	{
		// Cr�ation de la fen�tre
		super("Mon Application Graphiques"); //Titre de la fenetre
		setSize(1200, 1200); //Fenetre de taille 500*500
		setLocation(300, 200); //Position de la f�netre � 300 200
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		// R�cup�ration du container
		panneau = getContentPane();
		panneau.setLayout(new BorderLayout()); 
		
		// Cr�ation des objets � utiliser 
		nb_tour = new JLabel("  "); // Positionnement en haut, Vide car le contenu sera mise a jour par la m�thode next
		next = new JButton("Next"); //Positionnement du bouton en bas de la fenetre
		
		// Ajout de ces objets au panneau
		panneau.add(nb_tour, BorderLayout.NORTH); //Positionnement des �l�ments
		panneau.add(creerPlateau());              //Ajout de la m�thode, m�thode ici qui g�n�re le plateau
		panneau.add(next, BorderLayout.SOUTH);//Positionnement des �l�ments
		setVisible(true);//Rend visible le plateau
		next.addActionListener(this); //permet de rendre detectable l'action sur le bouton netx
		initFourmie();
		automatic();
		//automatic();//Appel de la m�thode automatic
	}
	
	//Exercice 1.5 Positionnement de la fourmi sur la case d�part
	public void initFourmie()
	   {
		plateau[x][y].setText(" " + formatfourmi);	
		//x et y sont ici donc les r�sultat de l'op�ration (dimension/2) soit 10/2 = 5
		//Afffichage au millieu de la grille
	  }
	//Fin Exercice 1.5
	public void next() 
	{
		nb_tour.setText(" NOMBRE DE TOURS : " + cpt);
		plateau[x][y].setText(""+direction);//Envoie la direction (nombre) sur chaque case de la grille
		//Si l'on se trouve sur case blanche
		if(plateau[x][y].getBackground()== Color.WHITE) //Si la position des points se situe sur une couleur blanche
		{
			plateau[x][y].setBackground(Color.BLACK); //Alors par convention elle devient noir
	    switch(direction) //On met un swith de maniere a affecter des instruction sur chaque
	    				  //cas de valeur de la variable direction
		{
		 case 0:   
			     {
					direction=270;	//En etant sur la case blanche elle effectue une rotation  � sa propre gauche
									//A partir de 0� on se retrouve donc � gauche 
					y = y-1; //On recule ensuite d'une case sur l'axe horizontale	
							//du point de vue de la grille
					cpt++; //Incr�mentation du compteur
			      }
		          break ;
		           	
		case 90:
		    	{
		    		direction=0;//En etant sur la case blanche elle effectue une rotation  � sa propre gauche
								//A partir de 90� on se retrouve donc a 0� en haut
		    		x = x-1;    //On monte donc d'une case
		    					//du point de vue de la grille
		    		cpt++;
		    	}
		          break;
		case 180:
		    	{
		    		direction=90;//En etant sur la case blanche elle effectue une rotation  � sa propre gauche
		    					//A partir de 180� on se retrouve donc a 90� � gauche
		    		y=y+1;		//On avance donc d'une case
		    					//du point de vue de la grilles
		    		cpt++;
		        }
		          break ;
		case 270:	
		    	{
		            direction=180;//En etant sur la case blanche elle effectue une rotation  � sa propre gauche
		            			//A partir de 270� on se retrouve donc a 180� � en bas
		    		x=x+1;		//on descend donc d'une case
		    					//du point de vue de la grille
		    		cpt++;
		    	}
		         break;
		        } 
		}
		//Si on est maintenant sur case noir
		if(plateau[x][y].getBackground()==Color.BLACK) 
		{
			plateau[x][y].setBackground(Color.WHITE);//Par convention elle devient blanche
	    switch(direction) 
		{
	    case 0:     
				{
					direction=90;//En etant sur la case noir elle effectue une rotation  � sa propre droite
								//A partir de 0� on se retrouve donc a 90� � gauche
					y = y+1;	//On avance alors d'une case du point de vue de la crille
					cpt++;
				}
			      break ;           	
			     //Meme proceder d'ex�cution pour les autre cas valeur de direction
	    case 90:
			    {
			    	direction=180;
			    	x = x+1;
			    	cpt++;
			    }
			       break;
		case 180:
			    {
			    	direction=270;
			    	y=y-1;
			    	cpt++;
			    }
			        break ;
		case 270:
			    {
			    	direction=0;
			    	x=x-1;
			    	cpt++;
			    }
			       break;	 
		} 
		}				
	}

	//Action effectuer losqu'on clique sur le bouton
	public void actionPerformed(ActionEvent click_next) //A chaque appuie sur le bouton next
	{
		if (click_next.getSource() == next) //Si on click sur le bouton next
		{
			
			
			//Exercice 1.7	
			automatic();
			//Fin
		}
	}			
		
	public void automatic() //Ajout de la m�thode automatic qui appele la m�thode next(); toutes
							//les 2ms
	{		
		try {
			while(x<dimension || y<dimension ) //Tant que x est inf�rieur a 100 et Y inf�rieur a 100
											  //C'est-�-dire tant qu'il sont inf�rieur a la dimension fenetre 
			{
				next();	//On ex�cute next
				Thread.sleep(2); //Toute les 2 milliseconde
			}
		} 
		catch (InterruptedException e)
		{
			e.printStackTrace();
		}
	}
	//Execution du Programme
			public static void main(String[] args) 
			{
				LaFourmiDeLangton1 n = new LaFourmiDeLangton1();
			}
	
}
